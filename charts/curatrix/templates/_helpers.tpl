{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
*/}}
{{- define "fullname" -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{- define "kc.auth_service.secret_volume_name" -}}
privatekey
{{- end -}}
{{- define "kc.auth_service.private_key_path" -}}
privatekey
{{- end -}}
{{- define "kc.auth_service.health_port" -}}
4000
{{- end -}}
{{- define "kc.auth_service.internal_port" -}}
3000
{{- end -}}
{{- define "kc.auth_service.mount_path" -}}
/etc/jwt_keys
{{- end -}}
